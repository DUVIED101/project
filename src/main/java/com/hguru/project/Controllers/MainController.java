package com.hguru.project.Controllers;


import com.hguru.project.models.Role;
import com.hguru.project.models.User;
import com.hguru.project.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.Map;

@Controller
public class MainController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/profile")
    @PreAuthorize("hasAnyAuthority('ADMIN','USER')")
    public String home(Model model) {
        model.addAttribute("title", "Профиль");
        return "home";
    }

    @GetMapping("/signing")
    public String reg(Model model) {
        model.addAttribute("title", "Аутентификация");
        return "reg";
    }

    @PostMapping("/signing")
    public String addUser(User user, Map<String, Object> model) {
       User userFromDb = userRepository.findByUsername(user.getUsername());

       if (userFromDb != null){
            model.put("message", "User exists");
            return("reg");
       }
        user.setRoles(Collections.singleton(Role.USER));
       userRepository.save(user);
        return "redirect:/login";
    }

    @GetMapping("/")
    public String about(Model model) {
        model.addAttribute("title", "Про нас");
        return "about";
    }
    @GetMapping("/about")
    @PreAuthorize("hasAnyAuthority('ADMIN','USER')")
    public String aboutfu(Model model) {
        model.addAttribute("title", "Про нас");
        return "aboutfu";
    }
}
