package com.hguru.project.Controllers;


import com.hguru.project.models.Card;
import com.hguru.project.repo.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Optional;

@Controller
public class CardController {

    @Autowired
    private CardRepository cardRepository;

    @GetMapping("/cards")
    @PreAuthorize("hasAnyAuthority('ADMIN','USER')")
    public String cardMain(Model model){
        Iterable<Card> cards = cardRepository.findAll();
        model.addAttribute("cards", cards);
        return "card-main";
    }

    @GetMapping("/cards/add")
    @PreAuthorize("hasAnyAuthority('ADMIN','USER')")
    public String cardAdd(Model model) {
        return "card-add";
    }

    @PostMapping("/cards/add")
    public String cardPostAdd(@RequestParam String appearance,@RequestParam String radical, @RequestParam String translation, Model model)
    {
        Card card = new Card(appearance, radical, translation);
        cardRepository.save(card);
        return "redirect:/cards";
    }

    @GetMapping("/cards/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN','USER')")
    public String cardDetail(@PathVariable(value = "id") long id, Model model) {
        if(!cardRepository.existsById(id)){
            return "redirect:/cards";
        }
        Optional<Card> card = cardRepository.findById(id);
        ArrayList<Card> res = new ArrayList<>();
        card.ifPresent(res::add);
        model.addAttribute("card", res);
        return "card-details";
    }

    @GetMapping("/cards/{id}/edit")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String cardRedact(@PathVariable(value = "id") long id, Model model) {
        if(!cardRepository.existsById(id)){
            return "redirect:/cards";
        }
        Optional<Card> card = cardRepository.findById(id);
        ArrayList<Card> res = new ArrayList<>();
        card.ifPresent(res::add);
        model.addAttribute("card", res);
        return "card-edit";
    }

    @PostMapping("/cards/{id}/edit")
    public String cardPostEd(@PathVariable(value = "id") long id, @RequestParam String appearance,@RequestParam String radical, @RequestParam String translation, Model model)
    {
        Card card = cardRepository.findById(id).orElseThrow();
        card.setAppearance(appearance);
        card.setRadical(radical);
        card.setTranslation(translation);
        cardRepository.save(card);

        return "redirect:/cards";
    }

    @PostMapping("/cards/{id}/remove")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String cardPostDel(@PathVariable(value = "id") long id, Model model)
    {
        Card card = cardRepository.findById(id).orElseThrow();
        cardRepository.delete(card);
        return "redirect:/cards";
    }
}

