package com.hguru.project.models;

public enum Role {
    USER, ADMIN;
}
